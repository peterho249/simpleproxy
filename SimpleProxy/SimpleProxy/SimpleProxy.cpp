﻿// SimpleProxy.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "SimpleProxy.h"
#include <afxsock.h>
#include <errhandlingapi.h>
#include <atlconv.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define  BUFFER_SIZE 512

// The one and only application object

CWinApp theApp;

using namespace std;

// Get host name and remote port from packet
// buffer [in] content of packet
// hostName [out] host name string
// port [out] remote port number
void GetHostNameAndRemotePort(char* buffer, char* hostName, unsigned int &port);

int main()
{
    int nRetCode = 0;

    HMODULE hModule = ::GetModuleHandle(nullptr);

    if (hModule != nullptr)
    {
        // initialize MFC and print and error on failure
        if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
        {
            // TODO: change error code to suit your needs
            wprintf(L"Fatal Error: MFC initialization failed\n");
            nRetCode = 1;
        }
        else
        {
            // TODO: code your application's behavior here.

			// Socket to receive data from client host
			// Send: from proxy to client
			// Receive: from client to client
			CSocket serverSocket;

			// Socket to listen on proxy server
			CSocket listenSocket;

			// Socket to send data to remote host 
			// Send: from proxy to remote
			// Receive: from remote to proxy
			CSocket clientSocket;

			//Proxy port
			unsigned int proxyPort = 8888;
			unsigned int remotePort = 80;

			//Buffer for packet send and receive
			char buffer[BUFFER_SIZE];

			// Connection succeed flag
			int connectFlag;

			AfxSocketInit(NULL);

			listenSocket.Create(proxyPort);
			listenSocket.Listen();
			listenSocket.Accept(serverSocket);
			while (true)
			{
				//listenSocket.Listen();
				//listenSocket.Accept(serverSocket);

				if (serverSocket.Receive(buffer, BUFFER_SIZE) <= 0)
				{
					printf("Cannot receive packet from client. Error code: %d\n", GetLastError());
					break;
				}

				// Xử lý packet, get host và port, nếu không có port chỉ định thì default = 80
				// Host name string
				char hostName[BUFFER_SIZE];
				GetHostNameAndRemotePort(buffer, hostName, remotePort);


				// Send packet từ proxy tới remote qua host và port từ packet. clientSocket
				clientSocket.Create();
				connectFlag = clientSocket.Connect(CA2W(hostName), remotePort);
				int countSuccess = 0;
				if (connectFlag)
				{
					clientSocket.Send(buffer, BUFFER_SIZE);

					// Receive packet từ remote về proxy. clientSocket
					if (clientSocket.Receive(buffer, BUFFER_SIZE) <= 0)
					{
						printf("Cannot receive packet. Error code: %d\n", GetLastError());
						break;
					}

					printf("%d packet send succeed.\n", ++countSuccess);

					clientSocket.Close();
				}
				else
				{
					printf("Cannot connect to remote server. Error code: %d\n", GetLastError());
					break;
				}
				
				// Send packet vừa nhận được về client. serverSocket
				if (connectFlag)
				{
					serverSocket.Send(buffer, BUFFER_SIZE);
				}

			}
			
			serverSocket.Close();
			listenSocket.Close();
        }
    }
    else
    {
        // TODO: change error code to suit your needs
        wprintf(L"Fatal Error: GetModuleHandle failed\n");
        nRetCode = 1;
    }

    return nRetCode;
}


void GetHostNameAndRemotePort(char* buffer, char* hostName, unsigned int &port)
{
	// Port number string
	// TODO: Convert this string to int and store as remotePort
	char portString[6];
	int index = 0;

	portString[0] = 0;
	int j = 0;
	bool flag = false;
	while (buffer[index] != '\r')
	{
		if (buffer[index] == ':' && buffer[index + 1] != '/')
		{
			flag = true;
			index++;
			continue;
		}

		if (flag)
		{
			if (buffer[index] == ' ')
				break;
			portString[j] = buffer[index];
			j++;
		}
		index++;
	}
	portString[j] = 0;

	for (; (buffer[index] == ':' && buffer[index - 1] == 't' && buffer[index - 2] == 's' &&
		buffer[index - 3] == 'o' && buffer[index - 4] == 'H') == 0; index++);

	index += 2;
	j = 0;
	hostName[0] = 0;
	
	while (buffer[index] != ':' && buffer[index] != '\r')
	{
		// Get host string
		hostName[j] = buffer[index];
		j++;
		index++;
	}
	hostName[j] = 0;

	if (strlen(portString) == 0)
		port = 80;
	else
		port = atoi(portString);
}